#include<stdio.h>
#include<string.h>
#define size 100
//Program to reverse a sentence entered by user

int main()
{
    char arr[size];
    int i;
    printf("Enter a sentence :");//Enter string
    scanf("%s",arr);

    printf("Print the reversed sentence :\n");//Reverse
    for(i=strlen(arr)-1; i>=0; i--)
        printf("%c",arr[i]);

    return 0;
}
